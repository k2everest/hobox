var express = require('express');
var router = express.Router();
var jwt = require("jsonwebtoken");

/* GET users listing. */
/* router.get('/', function(req, res, next) {
  res.send('respond with a resource');
}); */

router.post('/authenticate', function(req, res) {
  var db = req.db;
  db.collection('user').findOne({email: req.body.email, password: req.body.password}, function(err, user) {
          if (err) {
              res.json({
                  type: false,
                  data: "Error occured: " + err
              });
          } else {
              if (user) {
                 res.json({
                      type: true,
                      data: user,
                      token: user.token
                  });
              } else {
                  res.json({
                      type: false,
                      data: "Incorrect email/password"
                  });
              }
          }
      });
  });

  router.get('/', function(req, res) {
    var db = req.db;
    db.collection('user').find().toArray(function (err, items) {
      res.json(items);
    });
  });
  

  router.post('/add', function(req, res) {
    var db = req.db;
    db.collection('user').findOne({email:req.body.email, password: req.body.password}, function(err, user) {
            if (err) {
                res.json({
                    type: false,
                    data: "Error occured: " + err
                });
            } else {
                if (user) {
                    res.json({
                        type: false,
                        data: "User already exists!",
                        user: user
                    });
                } else {
                  
                  //db.collection('user').insert({email:req.body.email, password: req.body.password,token:0}, function(err, user) {
                    db.collection('user').save({email:req.body.email, password: req.body.password, token: jwt.sign( req.body, 'testeDeChave')},function(err, res) {
                            res.json({
                                type: true,
                                data: res,
                                token: token
                            })
                        //})
                    });
                }
            }
        });
    });

    router.get('/me', ensureAuthorized, function(req, res) {
      User.findOne({token: req.token}, function(err, user) {
              if (err) {
                  res.json({
                      type: false,
                      data: "Error occured: " + err
                  });
              } else {
                  res.json({
                      type: true,
                      data: user
                  });
              }
          });
      });

      function ensureAuthorized(req, res, next) {
        var bearerToken;
            var bearerHeader = req.headers["authorization"];
            if (typeof bearerHeader !== 'undefined') {
                var bearer = bearerHeader.split(" ");
                bearerToken = bearer[1];
                req.token = bearerToken;
                next();
            } else {
                res.send(403);
            }
        }
      

module.exports = router;
