var express = require('express');
var router = express.Router();
var jwt = require("jsonwebtoken");
var userHandlers = require('../controllers/userController.js');


/* GET users listing. */
/* router.get('/', function(req, res, next) {
  res.send('respond with a resource');
}); */
    //add new user
    router.post('/add',userHandlers.register);

    router.post('/signIn',userHandlers.sign_in);

    router.get('/', function(req, res) {
        return res.json({
            id:'1',
            firstName: 'string',
            lastName: 'string'
        });
    });
    /* var db = req.db;
    db.collection('user').find().toArray(function (err, items) {
        //res.json(items);
        res.json({id:'1',
            firstName: 'string',
            lastName: 'string'});
    }); */
    

    router.post('/', function(req, res) {
        var db = req.db;
        db.collection('user').find().toArray(function (err, items) {
            res.json(items);
        });
    });

    router.get('/me', ensureAuthorized, function(req, res) {
      User.findOne({token: req.token}, function(err, user) {
              if (err) {
                  res.json({
                      type: false,
                      data: "Error occured: " + err
                  });
              } else {
                  res.json({
                      type: true,
                      data: user
                  });
              }
          });
      });

      function ensureAuthorized(req, res, next) {
        var bearerToken;
            var bearerHeader = req.headers["authorization"];
            if (typeof bearerHeader !== 'undefined') {
                var bearer = bearerHeader.split(" ");
                bearerToken = bearer[1];
                req.token = bearerToken;
                next();
            } else {
                res.send(403);
            }
        }
      

module.exports = router;
