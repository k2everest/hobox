var mongo = require('mongoskin');
var Schema       = mongo.Schema;
 
var UserSchema   = new Schema({
    email: String,
    password: String,
    token: String
});
 
module.exports = mongo.model('User', UserSchema);