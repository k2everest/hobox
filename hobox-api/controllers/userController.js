'use strict';
//var mongoose = require('mongoose'),
  var jwt = require('jsonwebtoken'),
  bcrypt = require('bcrypt-nodejs');
 // User = mongoose.model('User');

exports.register = function(req, res) {
  var db = req.db;
  db.collection('user').findOne({email:req.body.email}, 
    function(err, user) {
        if (err) {
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (user) {
                res.json({
                    type: false,
                    data: "User already exists!",
                    user: user
                });
            } else { 
                var salt = bcrypt.genSaltSync(10); 
                var hash_password = bcrypt.hashSync(req.body.password, salt);            
                db.collection('user').save({fullName: req.body.fullName, email:req.body.email, password: hash_password}, function(err, user) {
                    if (err) {
                    return res.status(400).send({
                        message: err
                    });
                    } else {
                    user.hash_password = undefined;
                    return res.json(user);
                    }
                });
            }
        }
    });
};

exports.sign_in = function(req, res) {
    var db = req.db;
    console.log(req.body.user.email);
    db.collection('user').findOne({ email: req.body.user.email},function(err, user) {
        if (err) throw err;
        if (!user || !bcrypt.compareSync(req.body.user.password, user.password)) {
            return res.status(401).json({ message: 'Authentication failed. Invalid user or password.' });
        }
        return res.json({ token: jwt.sign({ email: user.email, fullName: user.fullName, _id: user._id }, 'RESTFULAPIs') });
    });
};

exports.loginRequired = function(req, res, next) {
  if (req.user) {
    next();
  } else {
    return res.status(401).json({ message: 'Unauthorized user!' });
  }
};
