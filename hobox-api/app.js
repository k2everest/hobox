var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwt = require("jsonwebtoken");


var index = require('./routes/index');
var users = require('./routes/users');


// Database
var mongo = require('mongoskin');
var db = mongo.db("mongodb://localhost:27017/api_hub", {native_parser:true});


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('superNode-auth','RESTFULAPIs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// DB torna-se acessível
app.use(function(req,res,next){
  req.db = db;
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, DELETE, PUT');
  res.setHeader('Access-Control-Allow-Headers', 'x-access-token, X-Requested-With,content-type, Authorization');
  next();
});
app.use(function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    //console.log(req.headers);
    console.log(token);
    
        if(token) {
            jwt.verify(token, app.get('superNode-auth'), function(err, decoded) {      
                if (err) {
                    return res.json({ success: false, message: 'Falha ao tentar autenticar o token!' });    
                } else {
                //se tudo correr bem, salver a requisição para o uso em outras rotas
                req.decoded = decoded;    
                //next();
                //return res.json({ success: true, message: 'autenticado!' });
                next(); 
                }
            });
    
            } else {
                next(); 
            // se não tiver o token, retornar o erro 403
           /*  return res.status(200).send({ 
                success: false, 
                message: 'Não há token.' 
            });  */      
        }
  }); 
app.use('/users/', users);
 


app.use('/', index);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
