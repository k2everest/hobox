import * as React from 'react';
import * as classNames from 'classnames';

const s = require('./style.scss');

interface Props {
  onClick?: () => void
  title: string
  enabled?: boolean
  submit?: boolean
  delete?: boolean
}

export default class Button extends React.Component<Props, {}> {
  static defaultProps = {
    enabled: true,
    submit: false
  }

  click(e: React.MouseEvent<HTMLButtonElement>) {
    if (!this.props.onClick) {
       return ;
    }
    e.stopPropagation();
    if (this.props.enabled) {
      this.props.onClick();
    }
  }

  render() {
    const className = classNames(s.root, {
      [s.disabled]: !this.props.enabled
    });

    return <button type={this.props.enabled && this.props.submit ? 'submit' : 'button'}
            className={this.props.delete ? s.delete : className}
            onClick={ev => this.click(ev)}>
            {this.props.title}
    </button>;
  }
}