import * as _superagent from 'superagent';
import * as superagentPromise from 'superagent-promise';
import {Config} from '../containers/App/Config';
import commonStore from '../stores/commonStore';
import authStore from '../stores/authStore';



const API_ROOT = Config.BASE_URL;
const superagent = superagentPromise(_superagent, global.Promise);




const tokenPlugin = req => {
    if (commonStore.token) {
      req.set('x-access-token', commonStore.token);
    }
  };

const handleErrors = err => {
    if (err && err.response && err.response.status === 401) {
      authStore.logout();
    }
    return err;
};

const responseBody = res => res.body;

const requests = {
    del: url =>
      superagent
        .del(`${API_ROOT}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    get: url =>
      superagent
        .get(`${API_ROOT}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    put: (url, body) =>
      superagent
        .put(`${API_ROOT}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
    post: (url, body) =>
      superagent
        .post(`${API_ROOT}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
  };

  const Auth = {
    current: () =>
      requests.get('/user'),
    login: (email, password) =>
      requests.post('/users/signIn', { user: { email, password } }),
    register: (username, email, password) =>
      requests.post('/users', { user: { username, email, password } }),
    save: user =>
      requests.put('/user', { user })
  };

  export default {
    Auth
  };