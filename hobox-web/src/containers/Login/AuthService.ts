//import {Observable} from 'rx';
import {Login} from './model';
/* import {Rest} from '../App/Rest'; */
import {Config} from '../App/Config';
import axios from 'axios';

export class AuthService {
    
    login(login: Login, token) {
        
        
        var qs = require('qs');
        var key ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImEiLCJmdWxsTmFtZSI6bnVsbCwiX2lkIjoiNTlkNWFlYzNjMTMzZjUzNjQ0Y2YzNTM2IiwiaWF0IjoxNTA3MjExODUyfQ.BsMPiwXfi0hbMRVHIfuyQ-NEoc7uAxpq4Pk5I1b-oaE';
        console.log(login);
        axios.defaults.baseURL = Config.BASE_URL;
        axios.defaults.headers.common['x-access-token'] = key;
        //axios.defaults.headers.common['cache-control'] = 'no-cache';
        
        //axios.defaults.headers.common['Authorization'] = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImEiLCJmdWxsTmFtZSI6bnVsbCwiX2lkIjoiNTlkNWFlYzNjMTMzZjUzNjQ0Y2YzNTM2IiwiaWF0IjoxNTA3MjExODUyfQ.BsMPiwXfi0hbMRVHIfuyQ-NEoc7uAxpq4Pk5I1b-oaE';
        //axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        axios.post('/users/signIn', qs.stringify(login))
        .then(function(response){
            console.log(response.data.token)
             token = response.data.token;
             window.localStorage.setItem('token', token);
        }).catch(function(err){
            console.log(err);
        });
        //return Rest.doPost<Login,any>(`${Config.BASE_URL}/users/signIn`, login);
    }

}