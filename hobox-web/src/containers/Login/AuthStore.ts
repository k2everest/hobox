import {diInject} from '../App/DIContext';
import {AuthService} from './AuthService';
import {observable,reaction} from 'mobx';
import {Login} from './model';
// import {Observable} from 'rx';

export class AuthStore {
    @diInject() authService: AuthService;
    @observable token: string = null;
    

        login(login: Login) {
            this.authService.login(login, this.token);
            console.log(this.token);
        }

        constructor() {
            reaction(
              () => this.token,
              token => {
                if (token) {
                  console.log('entrou');
                  window.localStorage.setItem('token', token);
                } else {
                  window.localStorage.removeItem('token');
                }
              }
            );
          }
}