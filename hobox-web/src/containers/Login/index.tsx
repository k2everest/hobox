import * as React from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
//import { AuthStore } from './AuthStore';
import AuthStore  from '../../stores/authStore';
//import {diInject} from '../App/DIContext';

/**
 * Components
 */
import LoginForm, {LoginFormFields} from './LoginForm';

/**
 * Style
 */

const s = require('./style.scss');

interface ILoginProps {};

interface ILoginState {};

@observer
export default class Login extends React.Component<ILoginProps, ILoginState> {
    @observable error: string = '';
    authStore = AuthStore;
    
    onSubmit = (fields: LoginFormFields) => {
        this.authStore.setEmail(fields.user);
        this.authStore.setPassword(fields.password);
        this.authStore.login();
    }

    render() {
        return (
            <div>
                <div className={s.background}>
                    <div className={s.container}>
                        <div className={s.form}>
                            <h1>Login</h1>
                            {(this.authStore.errors === undefined ? null : (
                               
                                <div> Usuário ou senha incorretos</div>
                            ))}                            
                            <LoginForm onSubmit={this.onSubmit}/>
                        </div>
                    </div>
                </div>        
            </div>
        );
    }
};