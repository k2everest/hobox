import {Observable} from 'rx';
import {User, Transaction} from './model';
import {Rest} from '../App/Rest';
import {Config} from '../App/Config';
import axios from 'axios';


export class UserService {
   
    getUsers(context:any) : any{
        var key ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImEiLCJmdWxsTmFtZSI6bnVsbCwiX2lkIjoiNTlkNWFlYzNjMTMzZjUzNjQ0Y2YzNTM2IiwiaWF0IjoxNTA3MjExODUyfQ.BsMPiwXfi0hbMRVHIfuyQ-NEoc7uAxpq4Pk5I1b-oaE';
        axios.defaults.baseURL = Config.BASE_URL;
        axios.defaults.headers.common['x-access-token'] = key;
        return axios.get(`${Config.BASE_URL}/users`,context).then(users =>{
            context.push(users.data);
        }).catch(function(err){
            console.log(err);
        });
        
    }

    getUser(userId: number): Observable<User> {
        return Rest.doGet<User>(`${Config.BASE_URL}/users/${userId}`);
    }

    getUserTransactions(userId: number): Observable<Transaction[]> {
        return Rest.doGet<Transaction[]>(`${Config.BASE_URL}/users/${userId}/transactions`);
    }
}