export interface User {
    id: string;
    firstName: string;
    lastName: string;
}

export interface Transaction {
    id: number;
    description: string;
}