import * as React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'mobx-react';
import {Menu} from '../Route/Menu';
import Login from '../Login';
import {RouteStore} from '../Route/RouteStore';
import '../../styles/styles.scss';
import {diContext} from './DIContext';
import {UserService} from '../User/UserService';
import {AuthService} from '../Login/AuthService';
import {UserStore} from '../User/UserStore';
//import {AuthStore} from '../Login/AuthStore';
import AuthStore from '../../stores/authStore';
export class App extends React.Component<any, any> {

    constructor(props: any, context: any) {
        super(props, context);
        diContext.routeStore = new RouteStore();
        diContext.userStore = new UserStore();
        diContext.authStore = AuthStore;
        diContext.userService = new UserService();
        diContext.authService = new AuthService();
    }

    render() {
        return (
                <Provider {...diContext}>
                    <Router> 
                        <OutContainer>                   
                            <Switch>                           
                                <Route exact path="/login" component={ Login } />                               
                                <Container>                                
                                        {diContext.routeStore.routes
                                            .map(r => <Route key={r.mapping} exact path={r.mapping} component={r.component}/>)}
                                            {/* <Route  component={NotFound}/>    */} 
                                 </Container>
                            </Switch>
                        </OutContainer>
                    </Router>
                </Provider>
        )
    }
}

export const Container = (props: {children?: any}) =>
            <div className="row">
                <div className="col-xs-6 col-sm-3" id="sidebar">
                    <Menu />
                </div>
                <div className="col-xs-12 col-sm-9">
                    {props.children}
                </div>
            </div>;

export const OutContainer = (props: {children?: any}) =>
<div className="page-container">
    <div className="navbar navbar-default navbar-fixed-top" >
        <div className="container">
            <div className="navbar-header">
                <a className="navbar-brand" href="#">HOBOX</a>
            </div>
        </div>
    </div>
    <div className="container">
            <div className="row">
                {props.children}
            </div>
    </div>
</div>;

export const NotFound = () => <div>Page not found</div>;