/* import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css'; */
//import 'bootstrap/dist/css/bootstrap.min.css';
import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {App} from './containers/App';

ReactDOM.render(<App />, document.getElementById('root'));